<?php
// $Id: import_node.php 6473 2008-08-05 10:45:19Z contractor2 $
/**
 * @package Sift
 * @file
 * Provide a method of exporting and importing content
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * Works in the same way as import_node but accepts command line paramaters to make it useable cross projects
 * Pass in the following arguments
 *    filename
 *    hostname - i.e. dev.jwiley-microscopy.uat.sift.com
 *    path_to_drupal - i.e. /data/drupal
 */

$filename = $argv[1];
$hostname = $argv[2];
$path = $argv[3];

if (!$path) {
  $path = '/data/drupal';
}

// use this one for nice content ready to go
define(MODULE_NAME, 'export_import_sift');
define(INPUTDIR, dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'out' );

// Turn off all error reporting
// error_reporting(0);

// This is needed to get the correct settings.php file
$_SERVER['HTTP_HOST'    ] = $hostname;
$_SERVER['SCRIPT_NAME'  ] = '/index.php';

chdir($path);

require_once 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if ( !module_exists( MODULE_NAME )) {
  exit( 'Please enable module: '. MODULE_NAME ."\n" );
}

// Set the mask so that we do not have permission problems in the future
umask(0);

// act as a valid user
global $user;
$user = user_load( array( 'uid' => 1 ) );

drupal_set_message( "Import start time: " . date('r') );
drupal_set_message( "Inserting from " . $filename );

//build a node programatically
drupal_set_message( import_sift_xml( $filename, TRUE ) );

drupal_set_message( "Import end time: " . date( 'r') );

$messages = drupal_set_message();

if ( $messages[ 'error' ] ) {
  print( "Errors\n" );
  foreach ( $messages['error'] as $message ) {
    print( $message . "\n" );
  }
}

if ( $messages[ 'status' ] ) {
  print( "Status\n" );
  foreach ( $messages['status'] as $message ) {
    print( $message . "\n" );
  }
}
// vim: syntax=php
