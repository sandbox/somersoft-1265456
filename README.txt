// $Id: README.txt,v 1.1.2.1 2009/03/07 16:44:48 wesku Exp $

-- SUMMARY --
This module provides a mechanism via a single hook function to import and export
content data to and from a defined XML structure. Modules are responsible for
adding and extracting content their own part of the node structure. Simple
functions are provided to help with this and also to support adding content files.

Knowledge of XML is not required.


-- REQUIREMENTS --
None.


-- INSTALLATION --
Normal Drupal module installation.

1.  Extract the package into your site's modules directory.
2.  Enable the sift_imex modules at 'Administer >> Modules'.
3.  Configure access privileges at 'Administer >> access control'.

See http://drupal.org/node/70151 for further information.


-- CONFIGURATION --
There are two ways of using the module:


-- CUSTOMIZATION --
You have a number of options available at Administer / Site configuration / Addthis under Button image settings
 and Widget settings. Image settings control the button image and widget controls the drop down and window that is opened when user clicks on a link sharing service. More information on how to customize your AddThis button can be found at http://addthis.com/help/customize/custom-button/

 
-- ROADMAP --
Drupal 5.x version will get features that support basic supplied Drupal modules,
when time allows, and other community contributed support for other modules for
hook_sift_imex that is not supported by the module maintainers.

Drupal 6.x version will get features that support basic supplied Drupal modules,
when time allows, and other community contributed support for other modules for
hook_sift_imex that is not supported by the module maintainers.

Drupal 7.x version will be released soon.


-- SUPPORT --
All issues (e.g. support requests, bug reports) are to be submitted using the
drupal.org issue tracker.


-- CREDITS --
* Jean-Yves Rouffiac www.shimeril.com


-- CONTACT --
Current maintainers:
* Andrew Crook (somersoft) - http://drupal.org/user/1430798


