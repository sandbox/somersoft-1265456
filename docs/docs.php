<?php
// $Id: docs.php 197 2011-08-23 09:29:22Z andrewc $
/**
 * @file
 * This file contains no working PHP code; it exists to provide additional documentation
 * for doxygen as well as to document hooks in the standard Drupal manner.
 */

/**
 * @mainpage Export Import Manual
 *
 * This is gives an overview of the export import modules.
 *
 * Implementation of hook_sift_imex().
 *
 * @param string $op
 * Can take one of the following values
 *   - import_start
 *     Called at the start of importing all data
 *     Return NULL
 *   - import_node_start
 *     Called before importing a node.
 *     If the attribute action is 'update' or 'delete' then return mixed.
 *     An integer which is the matched new node number to change or
 *     delete.
 *     A floating point number where the integer part is the matched node and
 *     the decimal part is 1 - the probability that it is that node.
 *     For example
 *     39.0 is 100% probable that it is node 39.
 *     39.75 for that it is 25% (1-0.75) chance that it is node 39.
 *     It can be an array of integer or floating point numbers with the same
 *     meanings defined earlier.
 *     In all other cases if will be NULL.
 *   - import
 *     Called whilst importing a node.
 *     Either returns the node or NULL to halt following import operations for
 *     this XML.
 *   - import_after_node_save
 *     Called after the current node has been passed to the node_save($node)
 *     function.
 *     Return NULL.
 *   - import_node_end
 *     Called after importing a node.
 *     Return NULL.
 *   - import_end
 *     Called after all the nodes have been imported.
 *     Return NULL.
 *   - export_start
 *     Called at the start of exporting data.
 *     Return NULL.
 *   - export_node_start
 *     Called before exporting a node.
 *     Return NULL.
 *   - export
 *     Called whilst exporting a node.
 *     Either returns the node or NULL to halt following export operations for
 *     this XML.
 *   - export_node_end
 *     Called after exported a node.
 *     Return NULL.
 *   - export_end
 *     Called after all the nodes have been exported
 *     Return NULL.
 *   - id
 *     Called to translated an old id to a new id
 *     Return new id
 *
 *   @param stdClass &$node = NULL.
 *     During 'import' the node is extended with information passed in the $xml
 *     variable. This is similar to hook_load() when extracting data from the
 *     database.
 *     For all other values of action starting with 'import' then $node is NULL.
 *     During export node information is extracted and returned as XML.
 *     This is similar to hook_save() when saving information to the database.
 *
 *   @param DOMDocument | DOMNode &$xml = NULL
 *     During import, the node is extended with information passed in the
 *     DOMNode $xml variable. This is similar to hook_load() when extracting
 *     data from the database;
 *     During import_start, import_node_start, import_node_end and import_end,
 *     optionally extra information can be extract from the DOMDocument $xml
 *     variable.
 *     During export, node information is extracted and returned as XML in a
 *     DOMNode created by using DOMDocument $xml variable.
 *     During export_start, export_start_node, export_node_end and export_end,
 *     extra information can be returned as XML in a DOMNode created by using
 *     DOMDocument $xml variable.
 *
 *   @param string $node_type = NULL
 *     During 'import_node_start' this has the the action value.
 *     When it is not NULL it overrides the value '$node->type'.
 *     This is very useful when another module is providing the hook
 *     functionality for this type.
 *     See http://api.drupal.org/api/function/hook_node_info/5 for more
 *     information.
 *
 *   @param integer id = NULL
 *     In some hook implementation, the internal id in the XML has a new id in
 *     this system. This enable
 *
 *  XML structure
 *  <pre>
<siftData>
  <node
   id="internalNodeID"
   type="drupalType"
   version="drupalTypeVersion"
   action="actionValue"
  >
    <revision id="internalVID">
      <data name="fieldname">value</data>
    </revision>
    <extra module="moduleName" version="moduleVersion">
      <data name="fieldname">value</data>
    </extra>
    <data name="nodeStructureVariableName">value</data>
  </node>
  <extra module="moduleName" version="moduleVersion"></extra>
</siftData>
</pre>
 *
 *  actionValue
 *   - 'add'    append
 *   - 'delete' delete
 *   - 'update' change
 *
 *  Restriction
 *    - internalNodeID can not be '0'.
 */
