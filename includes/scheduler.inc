<?php
// $Id: path.inc 5574 2008-07-07 14:23:24Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the scheduler module
 *
 * @author M Dixon http://www.computerminds.co.uk
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('scheduler') && !function_exists('scheduler_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function scheduler_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {

    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'scheduler' . ' ' . $op );

    switch ( $op ) {
      case 'export':
          $extra_xml = $dom_document->createElement( 'extra'  );
          $extra_xml->setAttribute( 'module',  'scheduler' );
          $extra_xml->setAttribute( 'version', '0.1'       );
          $xml->appendChild( $extra_xml );

          _sift_imex_addData( $extra_xml, 'publish_on',    date('c', $node->publish_on)    );
          _sift_imex_addData( $extra_xml, 'unpublish_on',  date('c', $node->unpublish_on)  );

        break;

      case 'import':
        $xpath = new DOMXPath($dom_document);
        $items = $xpath->evaluate( 'extra[@module="scheduler"]', $xml);
        if ( is_object( $items ) ) {
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);
            $node->publish_on = strtotime( _sift_imex_extractData( $extra_xml, 'publish_on' ) );
            $node->unpublish_on = strtotime( _sift_imex_extractData( $extra_xml, 'unpublish_on' ) );
          }
        }
        break;

    }
  }
}
/**
 * @}
 */