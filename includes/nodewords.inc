<?php
// $Id: nodewords.inc 5574 2008-07-07 14:23:24Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the nodewords module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('nodewords') && !function_exists('nodewords_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function nodewords_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'nodewords' . ' ' . $op );

    switch ( $op ) {
      case 'export':
        if ( $node->nodewords ) {
          $extra_xml     = $dom_document->createElement( 'extra'    );
          $extra_xml->setAttribute( 'module'   , 'nodewords' );
          $extra_xml->setAttribute( 'version'  , '0.1'       );
          $xml->appendChild( $extra_xml );

          foreach ( $node->nodewords as $k => $v ) {
            _sift_imex_addData( $extra_xml, $k, $v);
          }
        }
        break;

      case 'import':
        $xpath = new DOMXPath($dom_document);
        $items = $xpath->evaluate( 'extra[@module="nodewords"]', $xml);
        if ( is_object( $items ) ) {
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);
            // go through all child nodes looking for data nodes.
            for ( $child_node_index = 0 ; $child_node_index < $extra_xml->childNodes->length ; $child_node_index++) {
              $child_node = $extra_xml->childNodes->item( $child_node_index );
              if ( $child_node->nodeType == XML_ELEMENT_NODE && $child_node->hasAttribute( 'name' )) {
                $node->nodewords[ $child_node->getAttribute( 'name' ) ] = $child_node->nodeValue;
              }
            }
          }
        }
        break;
    }
  }
}
/**
 * @}
 */