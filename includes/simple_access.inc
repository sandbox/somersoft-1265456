<?php
// $Id: simple_access.inc 5574 2008-07-07 14:23:24Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the simple access module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('simple_access') && !function_exists('simple_access_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function simple_access_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
  static $sa_groupids;

    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'simple_access' . ' ' . $op );

    switch ( $op ) {
      case 'export_start':
        $sa_groupids = array();
        break;

      case 'export':
        if ( $node->simple_access ) {
          $extra_xml     = $dom_document->createElement( 'extra'  );
          $extra_xml->setAttribute( 'module'   , 'simple_access' );
          $extra_xml->setAttribute( 'version'  , '0.1'           );
          $xml->appendChild( $extra_xml );

          foreach ( $node->simple_access as $k => $v ) {
            // Flag that we want this group later
            $sa_groupids[ $k ] = $k;

            // save the group
            $sa_groupXML = $dom_document->createElement( 'sa_node' );
            $sa_groupXML->setAttribute( 'groupid', $k );
            $extra_xml->appendChild( $sa_groupXML );
            foreach ( $node->simple_access[ $k ] as $k1 => $v1 ) {
              _sift_imex_addData( $sa_groupXML, $k1, $v1);
            }
          }
        }
        break;

      case 'export_end':
        if ( count($sa_groupids)) {
          $extra_xml     = $dom_document->createElement( 'extra'  );
          $extra_xml->setAttribute( 'module'   , 'simple_access' );
          $extra_xml->setAttribute( 'version'  , '0.1'           );
          $xml->documentElement->appendChild( $extra_xml );

          $results = db_query( "SELECT * FROM {simple_access_groups}");
          while ( $result = db_fetch_array( $results ) ) {
            if ( array_key_exists( $result['gid'], $sa_groupids ) ) {
              $sa_XML = $dom_document->createElement( 'sa_group' );
              $extra_xml->appendChild( $sa_XML );
              foreach ( $result as $k => $v ) {
                _sift_imex_addData( $sa_XML, $k, $v);
              }
            }
          }

          $results = db_query( "SELECT sa_r.gid, r.name FROM {simple_access_roles} sa_r INNER JOIN {role} r ON sa_r.rid=r.rid");
          while ( $result = db_fetch_array( $results ) ) {
            if ( array_key_exists( $result['gid'], $sa_groupids ) ) {
              $sa_XML = $dom_document->createElement( 'sa_roles' );
              $extra_xml->appendChild( $sa_XML );
              foreach ( $result as $k => $v ) {
                _sift_imex_addData( $sa_XML, $k, $v);
              }
            }
          }
        }
        break;

      case 'import_start':
        // TODO: import simple access groups
        break;

      case 'import':
        // TODO: import simple access groups permissions
      break;
    }
  }
}
/**
 * @}
 */