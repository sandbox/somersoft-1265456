<?php
// $Id: upload.inc 6367 2008-07-31 14:03:58Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the upload module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('upload') && !function_exists('upload_sift_imex' ) ) {
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function upload_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL) {
    static $files;

    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'upload' . ' ' . $op );

    switch ( $op ) {
      case 'export':
        if ( is_array( $node->files ) && count( $node->files ) ) {
          $extra_xml = $dom_document->createElement( 'extra'    );
          $extra_xml->setAttribute( 'module'   , 'upload' );
          $extra_xml->setAttribute( 'version'  , '0.1'    );
          $xml->appendChild( $extra_xml );

          foreach ($node->files as $fid => $file ) {
            _sift_imex_addFile( $extra_xml, $fid );
          }
        }
        break;

      case 'import_start':
        $files = array();
        break;

      case 'import':
        if ( variable_get( 'upload_' . $node_type, 0 ) ) {
          $xpath = new DOMXPath($dom_document);
          $items = $xpath->evaluate( 'extra[@module="upload"]', $xml);
          if ( is_object( $items ) ) {
            for ($i = 0; $i < $items->length; $i++) {
              $extra_xml = $items->item($i);
              // go through all child nodes looking for data nodes.
              for ( $child_node_index = 0 ; $child_node_index < $extra_xml->childNodes->length ; $child_node_index++) {
                $child_node = $extra_xml->childNodes->item( $child_node_index );
                if ( $child_node->nodeType == XML_ELEMENT_NODE
                    && $child_node->nodeName == 'file'
                    ) {

                  // Extract the information.
                  $file = _sift_imex_extractFile( $xml, $child_node );
                  drupal_write_record( 'files', $file );

                  // Create a new record
                  $node->files[$file->fid] = $file;
                }
              }
            }
          }
        }
        break;
    }
  }
}
/**
 * @}
 */