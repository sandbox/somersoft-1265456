<?php
// $Id: taxonomy.inc 6710 2008-08-08 15:48:04Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the ubercart product module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('uc_product') && !function_exists('uc_product_sift_imex' ) ) {
  function uc_product_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL) {


    // Track where we are
    $dom_document = _sift_imex_process_tracker( $xml, 'event' . ' ' . $op  );
    $product_types = module_invoke_all('product_types');
    switch ( $op ) {
      case 'export':
        if ( is_null($node_type)) {
          $node_type = $node->type;
        }
        if ( in_array($node_type, $product_types)) {
          $extra_xml     = $dom_document->createElement( 'extra'    );
          $extra_xml->setAttribute( 'module'   , 'uc_product'  );
          $extra_xml->setAttribute( 'version'  , '0.1'       );
          $xml->appendChild( $extra_xml );
          _sift_imex_addData( $extra_xml, 'model', $node->model);
          _sift_imex_addData( $extra_xml, 'list_price', $node->list_price);
          _sift_imex_addData( $extra_xml, 'cost', $node->cost);
          _sift_imex_addData( $extra_xml, 'sell_price', $node->sell_price);
          _sift_imex_addData( $extra_xml, 'weight', $node->weight);
          _sift_imex_addData( $extra_xml, 'weight_units', $node->weight_units);
          _sift_imex_addData( $extra_xml, 'length', $node->length);
          _sift_imex_addData( $extra_xml, 'width', $node->width);
          _sift_imex_addData( $extra_xml, 'height', $node->height);
          _sift_imex_addData( $extra_xml, 'length_units', $node->length_units);
          _sift_imex_addData( $extra_xml, 'pkg_qty', $node->pkg_qty);
          _sift_imex_addData( $extra_xml, 'default_qty', $node->default_qty);
          _sift_imex_addData( $extra_xml, 'unique_hash', $node->unique_hash);
          _sift_imex_addData( $extra_xml, 'ordering', $node->ordering);
          _sift_imex_addData( $extra_xml, 'shippable', $node->shippable);
        }
        break;

      case 'import':
        if ( is_null($node_type)) {
          $node_type = $node->type;
        }
        if ( in_array($node_type, $product_types)) {
          $xpath  = new DOMXPath( $dom_document );
          $items  = $xpath->evaluate( 'extra[@module="uc_product"]', $xml);
          if ( is_object( $items ) ) {
            $extra_xml = $items->item(0);
            $node->model = _sift_imex_extractData( $extra_xml, 'model');
            $node->list_price = _sift_imex_extractData( $extra_xml, 'list_price');
            $node->cost = _sift_imex_extractData( $extra_xml, 'cost');
            $node->sell_price = _sift_imex_extractData( $extra_xml, 'sell_price');
            $node->weight = _sift_imex_extractData( $extra_xml, 'weight');
            $node->weight_units = _sift_imex_extractData( $extra_xml, 'weight_units');
            $node->length = _sift_imex_extractData( $extra_xml, 'length');
            $node->width = _sift_imex_extractData( $extra_xml, 'width');
            $node->height = _sift_imex_extractData( $extra_xml, 'height');
            $node->length_units = _sift_imex_extractData( $extra_xml, 'length_units');
            $node->pkg_qty = _sift_imex_extractData( $extra_xml, 'pkg_qty');
            $node->default_qty = _sift_imex_extractData( $extra_xml, 'default_qty');
            $node->unique_hash = _sift_imex_extractData( $extra_xml, 'unique_hash');
            $node->ordering = _sift_imex_extractData( $extra_xml, 'ordering');
            $node->shippable = _sift_imex_extractData( $extra_xml, 'shippable');
          }
        }
        break;
    }

  }
}
/**
 * @}
 */