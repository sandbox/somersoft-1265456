<?php
// $Id: node.inc 6710 2008-08-08 15:48:04Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the node module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('node') && !function_exists('node_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function node_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL) {

    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'node' . ' ' . $op );

    switch ( $op ) {
      case 'export':
        $xml->appendChild( $dom_document->createComment( ' node ' . $op . ' ' ) );
        $xml->setAttribute( 'version', '6.0');

        _sift_imex_addData( $xml, 'title'     , $node->title                );
        _sift_imex_addData( $xml, 'uid'       , $node->uid                  );
        _sift_imex_addData( $xml, 'vid'       , $node->vid                  );
        _sift_imex_addData( $xml, 'status'    , $node->status               );
        _sift_imex_addData( $xml, 'created'   , date( 'c', $node->created ) );
        _sift_imex_addData( $xml, 'changed'   , date( 'c', $node->changed ) );
        _sift_imex_addData( $xml, 'promote'   , $node->promote              );
        _sift_imex_addData( $xml, 'moderate'  , $node->moderate             );
        _sift_imex_addData( $xml, 'sticky'    , $node->sticky               );
        _sift_imex_addData( $xml, 'comment'   , $node->comment              );

        // Drupal 6 extra node fields
        _sift_imex_addData( $xml, 'type'      , $node->type                 );
        _sift_imex_addData( $xml, 'language'  , $node->language             );
        _sift_imex_addData( $xml, 'tnid'      , $node->tnid                 );
        _sift_imex_addData( $xml, 'translate' , $node->translate            );

        // TODO: This should loop around all the revisions for this node
        // in the database.
        $revision = $node;

        $revision_xml  = $dom_document->createElement( 'revision' );
        $revision_xml->setAttribute( 'id'      , $node->vid    );
        $revision_xml->setAttribute( 'version' , '0.1'       );
        $xml->appendChild( $revision_xml );

        _sift_imex_addData( $revision_xml, 'uid'       , $revision->uid                    );
        _sift_imex_addData( $revision_xml, 'title'     , $revision->title                  );
        $normailised_text = _sift_imex_normalise_filepath($revision->body);
        _sift_imex_addData( $revision_xml, 'body'      , $normailised_text                 );
        $normailised_text = _sift_imex_normalise_filepath($revision->teaser);
        _sift_imex_addData( $revision_xml, 'teaser'    , $normailised_text                 );
        _sift_imex_addData( $revision_xml, 'log'       , $revision->log                    );
        _sift_imex_addData( $revision_xml, 'timestamp' , date( 'c', $revision->timestamp ) );
        _sift_imex_addData( $revision_xml, 'format'    , $revision->format                 );
        break;

      case 'import':
        $xml->appendChild( $dom_document->createComment( ' node ' . $op . ' ' ) );
        $node->created  = strtotime( _sift_imex_extractData( $xml, 'created'  ) );
        $node->changed  = strtotime( _sift_imex_extractData( $xml, 'changed'  ) );

        $node->title    = _sift_imex_extractData( $xml, 'title'     );
        $node->uid      = _sift_imex_extractData( $xml, 'uid'       );
        $vid            = _sift_imex_extractData( $xml, 'vid'       );  // Locally generated
        $node->status   = _sift_imex_extractData( $xml, 'status'    );
        $node->promote  = _sift_imex_extractData( $xml, 'promote'   );
        $node->moderate = _sift_imex_extractData( $xml, 'moderate'  );
        $node->sticky   = _sift_imex_extractData( $xml, 'sticky'    );
        $node->comment  = _sift_imex_extractData( $xml, 'comment'   );

        // Drupal 6 extra node fields
        if ( 1 ) { //@ TODO: test the version number
          $node->type       = _sift_imex_extractData( $xml, 'type'      );
          $node->language   = _sift_imex_extractData( $xml, 'language'  );
          $node->tnid       = _sift_imex_extractData( $xml, 'tnid'      );
          $node->translate  = _sift_imex_extractData( $xml, 'translate' );
        }

        // Revision Information
        $revision_xml    = _sift_imex_extractTag( $xml, 'revision[@id="'. $vid .'"]' );

        if ( $revision_xml ) {
          $node->timestamp  = strtotime( _sift_imex_extractData( $revision_xml, 'timestamp'  ) );

          // These are populated on save
          // $node->uid    = _sift_imex_extractData( $revision_xml, 'uid'     );
          // $node->title  = _sift_imex_extractData( $revision_xml, 'title'   );
          // $node->vid    = _sift_imex_extractData( $revision_xml, 'vid'     );

          $localised_text = _sift_imex_extractData( $revision_xml, 'body'  );
          $node->body   = _sift_imex_localise_filepath($localised_text);
          $localised_text = _sift_imex_extractData( $revision_xml, 'teaser');
          $node->teaser = $localised_text;
          $node->log    = _sift_imex_extractData( $revision_xml, 'log'     );
          $node->format = _sift_imex_extractData( $revision_xml, 'format'  );
        }
        else {
          drupal_set_message( '_node_sift_imex: can not find revision '
            . $vid
            . "\n"
            . $dom_document->saveXML( $xml )
            );
        }
        break;
    }
  }
}

/**
 * @}
 */