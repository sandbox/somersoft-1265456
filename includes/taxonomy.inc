<?php
// $Id: taxonomy.inc 6710 2008-08-08 15:48:04Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the taxonomy module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('taxonomy') && !function_exists('taxonomy_sift_imex' ) ) {
  /**
   * All the hooks are first
   */

  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function taxonomy_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
    static $vids;
    static $terms2;
    static $old_tid_2_new_tid;
    static $old_vid_2_new_vid;

    // Track where we are
    $dom_document = _sift_imex_process_tracker( $xml, 'taxonomy' . ' ' . $op  );

    switch ( $op ) {
    case 'id':
        return $old_tid_2_new_tid[ $id ];
        break;

      case 'export_start':
        $vids = array();
        break;

      case 'export':
        if ( count( $node->taxonomy ) ) {
          $extra_xml     = $dom_document->createElement( 'extra'    );
          $extra_xml->setAttribute( 'module'   , 'taxonomy'  );
          $extra_xml->setAttribute( 'version'  , '0.1'       );
          $xml->appendChild( $extra_xml );

          foreach ( $node->taxonomy as $termid ) {
            // remember this termid
            $vids[ $termid->vid ] = $termid->vid;

            $term_xml     = $dom_document->createElement( 'term' );
            $attr         = $dom_document->createAttribute( 'id' );
            $attr->value  = $termid->tid;
            $term_xml->appendChild( $attr );

            $extra_xml->appendChild( $term_xml );
          }
        }
        break;

      case 'export_end':
        if ( count($vids)) {
          $extra_xml     = $xml->createElement( 'extra'    );
          $extra_xml->setAttribute( 'module'   , 'taxonomy'  );
          $extra_xml->setAttribute( 'version'  , '0.1'       );
          $dom_document->documentElement->appendChild( $extra_xml );
          if ( module_exists('taxonomy_xml') ) {
            module_load_include('inc', 'taxonomy_xml', 'xml_format');
            foreach ( $vids as $vid ) {
              $data_xml     = $xml->createElement( 'data' );
              $tax_XML      = new DomDocument();
              $tax_XML->loadXML( taxonomy_xml_xml_create( $vid ) );

              /* TODO: Try to clear the nodes tag.
              $xpath = new DOMXPath($dom_document);
              $items = $xpath->evaluate( '/vocabulary/nodes', $tax_XML->documentElement );
              if ( is_object( $items ) && $items->length ) {
                for ($i = 0; $i < $items->length; $i++) {
                  $nodesXML = $items->item($i);
                  $nodesXML->nodeValue = '';
                }
              }
              */

              $data_xml->appendChild( $xml->importNode( $tax_XML->documentElement, TRUE ) );
              $extra_xml->appendChild( $data_xml );
            }
          }
        }
        else {
          _sift_imex_process_tracker( $xml, 'taxonomy_xml module not installed' );
        }
        break;

      case 'import_start':
        $old_vid_2_new_vid  = array();
        $old_tid_2_new_tid  = array();
        $terms2         = array();

        $xpath = new DOMXPath($dom_document);
        $items = $xpath->evaluate( 'extra[@module="taxonomy"]/data/vocabulary', $dom_document->documentElement );

        if ( is_object( $items ) && $items->length ) {
          for ($i = 0; $i < $items->length; $i++) {
            $vocabulary_xml = $items->item($i);

            $oldvids = $xpath->evaluate( 'vid', $vocabulary_xml );
            if ( is_object( $oldvids ) && $oldvids->length ) {
              $oldvid = $oldvids->item(0)->nodeValue;
            }

            // Get the term name
            $names   = $xpath->evaluate( 'name', $vocabulary_xml );
            if ( is_object( $names ) && $names->length ) {
              $name = $names->item(0)->nodeValue;
            }

            $vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE LOWER('%s') LIKE LOWER(name)", trim($name)));
            if ( function_exists( 'taxonomy_xml_parse' )) {
              taxonomy_xml_parse($dom_document->saveXML( $vocabulary_xml ), $vid);
            }
            else {
              drupal_set_message( t( 'taxonomy_sift_imex: Please install taxonomy_xml module'), 'error' );
            }
            $vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE LOWER('%s') LIKE LOWER(name)", trim($name)));

            $old_vid_2_new_vid[ $oldvid ] = $vid;

            $oldterms = $xpath->evaluate( 'term', $vocabulary_xml );
            if ( is_object( $oldterms ) && $oldterms->length ) {
              for ($it = 0; $it < $oldterms->length; $it++) {
                $term_xml = $oldterms->item($it);

                // Get the old term tid
                $oldtids = $xpath->evaluate( 'tid', $term_xml );
                if ( is_object( $oldtids ) && $oldtids->length ) {
                  $oldtid = $oldtids->item(0)->nodeValue;
                }

                // Get the term name
                $names   = $xpath->evaluate( 'name', $term_xml );
                if ( is_object( $names ) && $names->length ) {
                  $name = $names->item(0)->nodeValue;
                }

                // Create the lookup table
                $old_tid_2_new_tid[ $oldtid ] = _taxonomy_get_term_by_name( $vid, $name );
              }
            }
          }
        }
        break;

      case 'import':
        $xpath  = new DOMXPath($dom_document);
        $items  = $xpath->evaluate( 'extra[@module="taxonomy"]', $xml);
        if ( is_object( $items ) && $items->length ) {
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);
            for ( $child_node_index = 0 ; $child_node_index < $extra_xml->childNodes->length ; $child_node_index++) {
              $child_node = $extra_xml->childNodes->item( $child_node_index );
              if ( $child_node->nodeType == XML_ELEMENT_NODE
                    && $child_node->nodeName == 'term'
                    ) {
                if ( $child_node->hasAttribute( 'id' )) {
                  $terms[] = $old_tid_2_new_tid[ $child_node->getAttribute( 'id' ) ];
                }
                else{
                  $vocabulary_name = _sift_imex_extractData( $child_node, 'VocabularyName' );
                  if ( $vid = taxonomy_get_vocabulary_by_name( $vocabulary_name ) ) {
                    $term_name = _sift_imex_extractData( $child_node, 'VocabularyTerm' );
                    if ( $term = _taxonomy_get_term_by_name( $vid, $term_name ) ) {
                      if ( !is_array( $terms ) || !in_array( $term, $terms ))
                        $terms[] = $term;
                    }
                    else{
                      drupal_set_message(
                        t( 'taxonomy_sift_imex: can not find term called')
                        .' '
                        . check_plain($term_name)
                        .' '
                        . t('in')
                        .' '
                        . check_plain($vocabulary_name)
                        );
                    }
                  }
                  else {
                    drupal_set_message(
                      t( 'taxonomy_sift_imex: unknown vocabulary called')
                      .': '
                      . check_plain($vocabulary_name)
                      );
                  }
                }
              }
            }
          }
          if ( $terms ) {
            if ( $node->nid ) {
              if ( !in_array( $terms['tid'], $node->taxonomy ) ) {
                taxonomy_node_save($node->nid, $terms);
              }
            }
            else {
              // Remember for end fixup
              $terms2[$xml->getAttribute( 'id' )] = $terms;
            }
          }
        }
        break;

      case 'import_after_node_save':
      // Same the terms for all those nodes with no node id.
        foreach ( $terms2 as $oldnid => $terms ) {
          $newnid = _sift_imex_oldnid2newnid( $oldnid );
          if ( $newnid ) {
            taxonomy_node_save( $newnid, $terms );
            unset( $terms2[ $oldnid ] );
          }
        }
        break;
    }
  }




  /**
   * For a given name, return the vid for it
   *
   * @param string $name
   * @return integer
   */
  if ( !function_exists('taxonomy_get_vocabulary_by_name')) {
    function taxonomy_get_vocabulary_by_name( $name ) {
      // Get all the vocabularies
      $vocabularies = taxonomy_get_vocabularies();

      // Check each Vocabulary
      foreach ( $vocabularies as $vocabulary ) {

        // To see if the name is the same
        if ( $vocabulary->name == $name ) {

          // If so then return that number
          return $vocabulary->vid;
        }
      }

      // Return a valid integer but not a valid vid.
      return 0;
    }
  }




  /**
   * Find the term id for a given term in a given vocabulary
   *
   * @param integer $vid
   * @param string $name
   * @return integer
   */
  if ( !function_exists('_taxonomy_get_term_by_name')) {
    function _taxonomy_get_term_by_name( $vid, $name ) {

      // Get all the terms with this name
      $terms = taxonomy_get_term_by_name($name);

      // Gothrough each term
      foreach ( $terms as $term ) {

        // If it is for this vocabulary
        if ( $term->vid == $vid ) {

            // Return the id
            return $term->tid;
        }
      }
      // Return a valid integer but not a valid tid.
      return 0;
    }
  }

}
/**
 * @}
 */
