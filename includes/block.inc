<?php
// $Id: block.inc 6350 2008-07-31 10:50:29Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the block module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 */
/**
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('block') && !function_exists('block_sift_imex' ) ) {
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param stdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function block_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL) {
    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'block' . ' ' . $op );

    switch ( $op ) {
      case 'export_start':
        if ( variable_get('sift_imex_export_blocks', FALSE ) ) {
          $dom_document->documentElement->appendChild( $dom_document->createComment( ' block ' . $op . ' ') );
          $sql = <<<END
SELECT DISTINCT
  block.delta,
  block.module,
  block.status,
  block.weight,
  block.region,
  block.custom,
  block.visibility,
  block.pages,
  box.body,
  box.info,
  box.format,
  box.bid
FROM
 {boxes} box
 INNER JOIN {blocks} block ON (box.bid = block.delta)
WHERE
  block.module = 'block'
END;
          $result= db_query($sql);
          while ( $results = db_fetch_object( $result)) {
            $block_xml = $dom_document->createElement( 'block' );
            foreach ( $results as $k => $v ) {
              switch ($k) {
                case 'nid':
                case 'cid':
                case 'bid':
                  //these dont get cloned
                  break;
                case 'body':
                    _sift_imex_addCData( $block_xml, $k, ($k == 'timestamp'? date( 'c', $v ) : $v ));
                  break;
                default:
                    _sift_imex_addData( $block_xml, $k, ($k == 'timestamp'? date( 'c', $v ) : $v ));
                break;
              }
            }
            $dom_document->firstChild->appendChild( $block_xml ); //stick it under siftdata
          }
        }
    break;
    }
  }




  /**
   * Creates a new block. (from crud.inc -> http://drupal.org/project/install_profile_api )
   * hopefully our import routine can call this..
   * @see http://drupal.org/project/install_profile_api
   * @param string $module
   * @param string $delta
   * @param string $theme
   * @param integer $status
   * @param integer $weight
   * @param string $region
   * @param integer $visibility
   * @param string $pages
   * @param integer $custom
   * @param integer $throttle
   * @param string $title
   */
  function install_add_block($module, $delta, $theme, $status, $weight, $region, $visibility = 0, $pages = '', $custom = 0, $throttle = 0, $title = '') {
    db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, region, visibility, pages, custom, throttle, title)
      VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, '%s', %d, %d, '%s')",
      $module, $delta, $theme, $status, $weight, $region, $visibility, $pages, $custom, $throttle, $title);
    if ($module == 'block') {
      $box = db_fetch_object(db_query('SELECT * FROM {boxes} WHERE bid=%d', $delta));
      db_query("INSERT INTO {boxes} (bid, body, info, format) VALUES (%d, '%s', '%s', '%s')", $box->bid, $box->body, $box->info, $box->format);
    }
  }
}

/**
 * @}
 */