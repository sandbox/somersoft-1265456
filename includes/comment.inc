<?php
// $Id: comment.inc 5574 2008-07-07 14:23:24Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the comment module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('comment') && !function_exists('comment_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function comment_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'comment' . ' ' . $op );

    switch ( $op ) {
      case 'export':
        $results = db_fetch_object( db_query('SELECT * FROM {node_comment_statistics} WHERE nid = %d', $node->nid ) );

        // Only add the extra tag if there are comments.
        if ( $results->comment_count ) {
          $extra_xml = $dom_document->createElement( 'extra' );
          $extra_xml->setAttribute( 'module'   , 'comment' );
          $extra_xml->setAttribute( 'version'  , '0.1'     );
          $xml->appendChild( $extra_xml );

          foreach ( $results as $k => $v ) {
            if ( $k != 'nid') {
              _sift_imex_addData( $extra_xml, $k, ($k == 'last_comment_timestamp'? date( 'c', $v ) : $v ) );
            }
          }

          $result = db_query('SELECT * FROM {comments} WHERE nid = %d', $node->nid);
          while ( $results = db_fetch_object( $result)) {
            $comment_xml = $dom_document->createElement( 'comment' );
            $extra_xml->appendChild( $comment_xml );

            foreach ( $results as $k => $v ) {
              if ( ( $k != 'nid') && ($k != "cid")) {
                _sift_imex_addData( $comment_xml, $k, ($k == 'timestamp'? date( 'c', $v ) : $v ));
              }
            }
          }
        }
        break;

      case 'import':
        // TODO: import comments
        break;
    }
  }
}
/**
 * @}
 */