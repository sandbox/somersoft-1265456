<?php
/**
 * @package Sift
 *
 * @file
 * Import export routines for the comment module
 *
 * @copyright Copyright 2011 AEA Group
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('imagepicker') && !function_exists('imagepicker_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function imagepicker_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
    global $_sift_imex_export_options;
    static $exported_images;

    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'imagepicker' . ' ' . $op );

    // List of
    $a_imagepicker_fields = array(
      'uid',
      'img_title',
      'img_description',
      'img_date'
      );

    switch ( $op ) {
      case 'export-start':
        $exported_images = array();
        break;

      case 'export':
        // Extract the imagepicker entered content
        $pattern = '#'. IMAGEPICKER_FILES_DIR .'/(\d+)/(.*?)["\']#i';
        $subject = $node->body;
        $matches = array();
        $match_count = preg_match_all($pattern, $subject, $matches);

        // Only add the extra tag if there are comments.
        if ( $match_count ) {
          $extra_xml = $dom_document->createElement( 'extra' );
          $extra_xml->setAttribute( 'module'   , 'imagepicker' );
          $extra_xml->setAttribute( 'version'  , '0.1'     );
          $xml->appendChild( $extra_xml );
          foreach ( $matches[2] as $img_name ) {
            $image_xml = $dom_document->createElement( 'imagepicker' );
            $image_xml->setAttribute( 'img_name', $img_name );
            $extra_xml->appendChild( $image_xml );

            // Check that the details are not already entered
            if ( !isset($exported_images[$img_name]) ) {
              $exported_images[$img_name] = 1;
              $s_sql = <<< SQL
SELECT
  *
FROM
  {imagepicker}
WHERE
  img_name = '%s'
SQL;
              $result = db_query_range($s_sql, $img_name, 0, 1);
              $img = db_fetch_array($result);
              foreach ($a_imagepicker_fields as $key ) {
                if ( $key == 'img_date') {
                  _sift_imex_addData( $image_xml, $key, date( 'c', $img[ $key ] ) );
                }
                else {
                 _sift_imex_addData( $image_xml, $key, $img[ $key ] );
                }
              }

              // Build the file path
              $filepath =
                file_directory_path()
                . DIRECTORY_SEPARATOR
                . IMAGEPICKER_FILES_DIR
                . DIRECTORY_SEPARATOR
                . $img['uid']
                . DIRECTORY_SEPARATOR
                . $img['img_name']
                ;

              // Add the attribute with the correct URL
              if ( $_sift_imex_export_options['files_by_value'] ) {
                // See if file is by value or ref
                _sift_imex_addData(
                  $image_xml,
                  'contents',
                  chunk_split(base64_encode(file_get_contents($filepath)))
                  );
              }
              else {
                // Create the URL for remote importing.
                if ( variable_get('file_downloads', '1') == 2 ) {
                  $full = imagepicker_get_image_path($img, 'full', array('uid' => $img->uid));
                }
                else {
                  $filepath =
                    file_directory_path()
                    . DIRECTORY_SEPARATOR
                    . IMAGEPICKER_FILES_DIR
                    . DIRECTORY_SEPARATOR
                    . $img['uid']
                    . DIRECTORY_SEPARATOR
                    . $img['img_name']
                    ;
                  $full = file_create_url( $filepath );
                }
                $image_xml->setAttribute( 'url', $full );
              }
            }
          }
        }
        break;

      case 'import':
        // Now we have the nid value
        // associate the other information.
        $xpath = new DOMXPath($dom_document);
        $items = $xpath->evaluate( 'extra[@module="imagepicker"]/imagepicker', $xml);
        if ( is_object( $items ) ) {
          for ($i = 0; $i < $items->length; $i++) {
            $image_xml = $items->item($i);

            // Check to see if this is the first or
            // after the first time in this XML
            if ( $image_xml->hasChildNodes() ) {
              // Extract the information
              $img = array();
              $img['img_name'] = $image_xml->getAttribute( 'img_name' );
              foreach ($a_imagepicker_fields as $key ) {
                $img[$key] = _sift_imex_extractData( $image_xml, $key, '' );
              }

              // Build the file path
              // code taken from imagepicker_upload_form_submit()
              $destdir    = imagepicker_get_path( FALSE, $img );
              $filepath = $destdir
                . DIRECTORY_SEPARATOR
                . $img['img_name']
                ;

              // If the file is present the do not overwrite
              if ( file_exists($filepath) ) {
                continue;
              }

              // Get the file contents
              $filesize = _sift_imex_extract_file_content_and_save(
                $dom_document,
                $image_xml,
                $filepath
                );

              $thumbsdir    = $destdir . IMAGEPICKER_THUMBS_DIR;
              $browserdir   = $destdir . IMAGEPICKER_BROWSER_DIR;
              $origdir      = $destdir . IMAGEPICKER_ORIG_DIR;
              $maxthumbsize = 100;

              file_check_directory($destdir, TRUE);
              file_check_directory($thumbsdir, TRUE);
              file_check_directory($browserdir, TRUE);
              file_check_directory($origdir, TRUE);

              module_load_include( 'inc', 'imagepicker', 'imagepicker.imagefuncs');

              // Add DIRECTORY_SEPARATORS here because
              // drupals' functions remove trailing slashes
              $thumbsdir    = $thumbsdir  . DIRECTORY_SEPARATOR;
              $browserdir   = $browserdir . DIRECTORY_SEPARATOR;
              $origdir      = $origdir    . DIRECTORY_SEPARATOR;

              // Create the thumb and browse versions
              imagepicker_scale_image(
                $filepath,
                $thumbsdir . $img['img_name'],
                $maxthumbsize
                );
              imagepicker_scale_image(
                $filepath,
                $browserdir . $img['img_name'],
                $maxthumbsize
                );

              // Save the image information in the database.
              imagepicker_insert_image(
                $img['uid'            ],
                $img['img_name'       ],
                $img['img_title'      ],
                $img['img_description']
                );
            }
          }
        }
       break;
    }
  }
}
/**
 * @}
 */