<?php
// $Id$
/**
 * @file
 * Basic CCK import / export
 * Initial version - M.Dixon July 2008
 *
 * @author M Dixon http://www.computerminds.co.uk
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @TODO :
 *  1.  Handle revisions
 *  2.  Imagefields
 *  3.  Node reference fields
 *  4.  User reference fields
 *  5.  Date module
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('content') && !function_exists('content_sift_imex' ) ) {
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function content_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL) {
    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'content' . ' ' . $op );
    switch ( $op ) {
      case 'export':
        //Get the fields for this node type from CCK
        $content_types = content_types();
        $content_type_info = $content_types[$node->type];

        $extra_xml     = $dom_document->createElement( 'extra'    );
        $extra_xml->setAttribute( 'module'   , 'content'  );
        $extra_xml->setAttribute( 'version'  , '0.1'       );
        $xml->appendChild( $extra_xml );

        if (count($content_type_info['fields'])) {
          foreach ($content_type_info['fields'] as $field_name => $field_info) {
            $field = $node->$field_name;
            if (is_array($field)) {

              foreach ($field as $delta => $value) {
                $field_xml      = $dom_document->createElement( 'data' );
                //the delta for this field
                $attr_delta         = $dom_document->createAttribute( 'delta' );
                $attr_delta->value  = $delta;
                $field_xml->appendChild( $attr_delta );

                //the field name
                $attr_name         = $dom_document->createAttribute( 'name' );
                $attr_name->value  = $field_name;
                $field_xml->appendChild( $attr_name );


                //Lots of CCK types have the format param
                if (isset($value['format'])) {
                  $attr_format         = $dom_document->createAttribute( 'format' );
                  $attr_format->value  = $value['format'];
                  $field_xml->appendChild( $attr_format );
                }

                //Link content type - handle title and URL fields
                if ($field_info['type']=='link') {
                  $attr_url         = $dom_document->createAttribute( 'url' );
                  $attr_url->value  = $value['url'];
                  $field_xml->appendChild( $attr_url );

                  $attr_title         = $dom_document->createAttribute( 'title' );
                  $attr_title->value  = $value['title'];
                  $field_xml->appendChild( $attr_title );
                }

                if ($field_info['type']=='date') {
                  //Handle the optional second date
                }
                //the actual field value
                $data = $dom_document->createTextNode( $value['value'] );
                $field_xml->appendChild( $data );

                $extra_xml->appendChild( $field_xml );
              }
            }
          }
        }

        break;

      case 'import':
        $content_types = content_types();
        $content_type_info = $content_types[$node->type];

        $xpath  = new DOMXPath($dom_document);
        $items  = $xpath->evaluate( 'extra[@module="content"]', $xml);
        if ( is_object( $items ) && $items->length ) {
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);
            for ( $child_node_index = 0 ; $child_node_index < $extra_xml->childNodes->length ; $child_node_index++) {
              $child_node = $extra_xml->childNodes->item( $child_node_index );
              if ( $child_node->nodeType == XML_ELEMENT_NODE && $child_node->nodeName == 'data' ) {
                if ( $child_node->hasAttribute( 'name' )) {
                  $field_name = $child_node->getAttribute( 'name' );
                  $delta = $child_node->getAttribute( 'delta' );
                  $value = $child_node->nodeValue;

                  $field_values = $node->$field_name;
                  $field_values[$delta]['value']=$value;

                  $node->$field_name = $field_values;
                }
              }
            }
          }
        }
        break;
    }
  }
}

/**
 * @}
 */