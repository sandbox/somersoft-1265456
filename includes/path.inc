<?php
// $Id: path.inc 5786 2008-07-11 08:13:30Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Import export routines for the path module
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('path') && !function_exists('path_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function path_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
  static $paths;

    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'path' . ' ' . $op );

    switch ( $op ) {
      case 'export':
        $results = db_query('SELECT * FROM {url_alias} WHERE src = \'%s\'', 'node/' . $node->nid );
        $extra_xml = NULL;
        while ( $result = db_fetch_object( $results)) {
          if ( is_null($extra_xml) ) {
            $extra_xml     = $dom_document->createElement( 'extra'  );
            $extra_xml->setAttribute( 'module'   , 'path' );
            $extra_xml->setAttribute( 'version'  , '0.1'           );
            $xml->appendChild( $extra_xml );
          }
          _sift_imex_addData( $extra_xml, 'urlAlias', $result->dst );
        }
        break;

      case 'import_start':
        $paths = array();
        break;

      case 'import':
        $xpath  = new DOMXPath( $dom_document );
        $items  = $xpath->evaluate( 'extra[@module="path"]', $xml);
        if ( is_object( $items ) ) {
          $oldnid  = $xml->getAttribute( 'id' );
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);
            $paths[$oldnid] = _sift_imex_extractData( $extra_xml, 'urlAlias' );
          }
        }
        break;

      case 'import_after_node_save':
        foreach ( $paths as $oldnid => $url_alias ) {
          $newnid = _sift_imex_oldnid2newnid( $oldnid );
          if ( $newnid ) {
            if ( is_array( $url_alias ) ) {
              foreach ( $url_alias as $dst ) {
                if ( module_exists( 'pathauto')) {
                  db_query( "DELETE FROM {url_alias} WHERE src = '%s'", 'node/' . $newnid );
                }
                db_query( "DELETE FROM {url_alias} WHERE dst = '%s'", $dst );
                db_query(
                  "INSERT INTO {url_alias} (src,dst) VALUES ('%s','%s')",
                  'node/' . $newnid
                  , $dst
                );
              }
            }
            else {
              if ( module_exists( 'pathauto')) {
                db_query( "DELETE FROM {url_alias} WHERE src = '%s'", 'node/' . $newnid );
              }
              db_query( "DELETE FROM {url_alias} WHERE dst = '%s'", $url_alias );
              db_query(
                  "INSERT INTO {url_alias} (src,dst) VALUES ('%s','%s')",
                  'node/' . $newnid
                  , $url_alias
                );
            }
            unset( $paths[ $oldnid ] );
          }
        }
        break;
    }
  }
}
/**
 * @}
 */