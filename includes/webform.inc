<?php
/**
 * @package Sift
 *
 * @file
 * Import export routines for the comment module
 *
 * @copyright Copyright 2011 AEA Group
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */

if ( module_exists('webform') && !function_exists('webform_sift_imex' ) ) {
  /**
   * All the hooks are first
   */
  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function webform_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL ) {
    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'webform' . ' ' . $op );

    // List of
    $a_webform_fields = array(
      'confirmation',
      'confirmation_format',
      'redirect_url',
      'status',
      'block',
      'teaser',
      'allow_draft',
      'auto_save',
      'submit_notice',
      'submit_text',
      'submit_limit',
      'submit_interval',
    );

    $a_webform_component_fields = array(
      'cid',
      'pid',
      'form_key',
      'name',
      'type',
      'value',
      'extra',
      'mandatory',
      'weight',
    );

    $a_webform_email_fields = array(
      'email',
      'subject',
      'from_name',
      'from_address',
      'template',
      'excluded_components',
      'html',
      'attachments',
    );

    switch ( $op ) {
      case 'export':
        // Get the webform configuration information
        $results = db_fetch_object( db_query('SELECT * FROM {webform} WHERE nid = %d', $node->nid ) );

        // Only add the extra tag if there are comments.
        if ( $results->status ) {
          $extra_xml = $dom_document->createElement( 'extra' );
          $extra_xml->setAttribute( 'module'   , 'webform' );
          $extra_xml->setAttribute( 'version'  , '0.1'     );
          $xml->appendChild( $extra_xml );

          foreach ( $results as $k => $v ) {
            if ( $k != 'nid') {
              _sift_imex_addData( $extra_xml, $k, $v );
            }
          }

          // get all the webform components
          $result = db_query('SELECT * FROM {webform_component} WHERE nid = %d ORDER BY cid', $node->nid);
          while ( $results = db_fetch_object( $result)) {
            $component_xml = $dom_document->createElement( 'component' );
            $extra_xml->appendChild( $component_xml );

            foreach ( $results as $k => $v ) {
              if ( $k != 'nid' ) {
                _sift_imex_addData( $component_xml, $k, $v );
              }
            }
          }

          // get all the webform roles
          $result = db_query('SELECT r.name FROM {webform_roles} wr INNER JOIN {role} r ON wr.rid=r.rid WHERE wr.nid = %d ORDER BY r.rid', $node->nid);
          while ( $results = db_fetch_object( $result)) {
            $role_xml = $dom_document->createElement( 'role' );
            $extra_xml->appendChild( $role_xml );

            foreach ( $results as $k => $v ) {
              if ( $k != 'nid' ) {
                _sift_imex_addData( $role_xml, $k, $v );
              }
            }
          }

          // get all the webform emails
          $result = db_query('SELECT * FROM {webform_emails} we WHERE we.nid = %d ORDER BY eid', $node->nid);
          while ( $results = db_fetch_object( $result)) {
            $email_xml = $dom_document->createElement( 'email' );
            $extra_xml->appendChild( $email_xml );

            foreach ( $results as $k => $v ) {
              if ( ($k != 'nid') && ($k != 'eid') ) {
                _sift_imex_addData( $email_xml, $k, $v );
              }
            }
          }
        }
        break;

      case 'import':
        // Now we have the nid value
        // associate the other information.
        $xpath = new DOMXPath($dom_document);
        $items = $xpath->evaluate( 'extra[@module="webform"]', $xml);
        if ( is_object( $items ) ) {
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);

            foreach ( $a_webform_fields as $v ) {
              $node->webform[$v] = _sift_imex_extractData( $extra_xml, $v );
            }

            // Insert the webform conponents
            $component_items = $xpath->evaluate( 'component', $extra_xml);
            if ( is_object( $component_items ) ) {
              for ($ci = 0; $ci < $component_items->length; $ci++) {
                $component_xml = $component_items->item($ci);
                $component = array();

                // Get the webform component values.
                foreach ( $a_webform_component_fields as $v ) {
                  $component[$v] = _sift_imex_extractData( $component_xml, $v );
                  if ( $v == 'extra' ) {
                    $component[$v] = unserialize($component[$v]);
                  }
                }
                $node->webform['components'][$component['cid']] = $component;
              }
            }

            // Insert the webform emails
            $node->webform['emails'] = array();
            $email_items = $xpath->evaluate( 'email', $extra_xml);
            if ( is_object( $email_items ) ) {
              for ($ei = 0; $ei < $email_items->length; $ei++) {
                $email_xml = $email_items->item($ei);
                $email = array();

                // Get the webform component values.
                foreach ( $a_webform_email_fields as $v ) {
                  $email[$v] = _sift_imex_extractData( $email_xml, $v );
                }
              $node->webform['emails'][($ei + 1)] = $email;
              }
            }

            // Insert the webform emails
            $node->webform['roles'] = array();
            $role_items = $xpath->evaluate( 'role', $extra_xml);
            if ( is_object( $role_items ) ) {
              for ($ri = 0; $ri < $role_items->length; $ri++) {
                $role_xml  = $role_items->item($ri);
                $role_name = _sift_imex_extractData( $role_xml, 'name' );

                // Do we have the rid value already
                if ( !isset( $roles[$role_name])) {
                  $results = db_query('SELECT rid FROM {role} r WHERE r.name = \'%s\'', $role_name );
                  while ( $result = db_fetch_object( $results ) ) {
                    $roles[$role_name] = $result->rid;
                  }
                }

                // Still not set then create one
                if ( !isset( $roles[$role_name])) {
                  $arole = array(
                    'name' => $role_name
                  );
                  drupal_write_record( 'role', $arole );
                  $roles[$role_name] = $arole['rid'];
                }

                $node->webform['roles'][] = $roles[$role_name];
              }
            }

          }
        }
        break;
    }
  }
}
/**
 * @}
 */