<?php
/**
 * @package Sift
 *
 * @file
 * Import export routines for the book module
 *
 * @author
 * Andrew Crook (somersoft) - http://drupal.org/user/1430798
 *
 * @addtogroup export_import_sift Export Import Drupal Module.
 *
 * @{
 */




if ( module_exists('book') && !function_exists('book_sift_imex' ) ) {

  /**
   * Implementation of hook_sift_imex().
   *
   * @param string $op
   * @param StdClass $node
   * @param DOMDocument|DOMNode $xml
   * @param integer $id
   * @return mixed
   */
  function book_sift_imex( $op, &$node = NULL, &$xml = NULL, $node_type = NULL, $id = NULL) {
    global $_sift_imex_export_options;




    /**
     * @var array
     */
    static $a_mlid_2_nid;




    /**
     * @var array
     */
    static $node_fixup;


    // Track where we are.
    $dom_document = _sift_imex_process_tracker( $xml, 'book' . ' ' . $op );

    switch ( $op ) {
      case 'export_start':
        $a_mlid_2_nid = array();
        // Extract the information that relates a mlid to a node first
        // incase the order of presented data is not top down for a book.
        $s_sql = <<< SQL
SELECT
  mlid,
  nid
FROM
  {book}
SQL;
        $result2 = db_query($s_sql);
        while ( $alink = db_fetch_array($result2) ) {
          $a_mlid_2_nid[$alink['mlid']] = $alink['nid'];
        }
        break;


      case 'export':
          if (isset($node->book) ) {
            $xml->appendChild( $dom_document->createComment( ' book ' . $op . ' ' ) );

            $extra_xml = $dom_document->createElement( 'extra'    );
            $extra_xml->setAttribute( 'module'   , 'book' );
            $extra_xml->setAttribute( 'version'  , '0.1'    );
            $xml->appendChild( $extra_xml );

            // Just add the weight
            _sift_imex_addData( $extra_xml, 'weight', $node->book['weight'] );

            // Add the plid information.
            // Now this is not the index the menu_links table, but the Node ID
            // If there is no parent then use 0
            $i_parent_node_id = isset( $a_mlid_2_nid[ $node->book['plid'] ] )
              ? $a_mlid_2_nid[ $node->book['plid'] ]
              : 0
              ;
            _sift_imex_addData( $extra_xml, 'plid', $i_parent_node_id );

            // If the parent is present then add book node and the parent node.
            // if there no parent node then this is the book node so now need to
            // add the information.
            if ( $node->book['plid'] ) {
              // Added the Book Node
              _sift_imex_addNode( $extra_xml, $node->book['bid'], 'bid' );

              // Only add the Parent Node if not the same as the Book Node
              // Cuts down the amount of XML
              if ( $node->book['bid'] != $i_parent_node_id ) {
                // Add the parent Node
                _sift_imex_addNode( $extra_xml, $i_parent_node_id, 'plid' );
              }
            }
            else {
              if ( $_sift_imex_export_options['associated'] ) {
                $tree = book_menu_subtree_data($node->book);
                $a_nodes = _sift_imex_book_export_traverse($tree);
                $book_structure_xml = $dom_document->createElement( 'book_structure' );
                $extra_xml->appendChild( $book_structure_xml );
                foreach ( $a_nodes as $nid ) {
                  _sift_imex_addNode( $book_structure_xml, $nid );
                }
              }
            }
          }
        break;


      case 'import_start':
        $node_fixup  = array();
        break;


      case 'import':
        $xpath = new DOMXPath($dom_document);
        // Import all the other nodes in the book if present
        $items = $xpath->evaluate( 'extra[@module="book"]/book_structure/node', $xml);
        if ( is_object( $items ) ) {
          for ($i = 0; $i < $items->length; $i++) {
            $node_xml = $items->item($i);
            _sift_imex_extractNode( $dom_document, $node_xml );
          }
        }
        break;


      case 'import_after_node_save':
        $xpath = new DOMXPath($dom_document);
        $items = $xpath->evaluate( 'extra[@module="book"]', $xml);
        if ( is_object( $items ) ) {
          for ($i = 0; $i < $items->length; $i++) {
            $extra_xml = $items->item($i);
            $weight = _sift_imex_extractData( $extra_xml, 'weight' );
            $plid   = _sift_imex_extractData( $extra_xml, 'plid'   );
            if ( $plid ) {

              // extract the book information
              $bid  = _sift_imex_extractNode( $dom_document, $extra_xml, 'bid' );

              $node_fixup[ $node->nid ] = array(
                'bid'    => $bid,
                'plid'   => $plid,
                'weight' => $weight,
              );

              // Optionally extract the parent node
              _sift_imex_extractNode( $dom_document, $extra_xml, 'plid' );
            }
            else {
              // Set the book information name
              $node->book['bid'   ] = 'new';
              $node->book['plid'  ] = 0;
              $node->book['weight'] = $weight;
              $node->book['module'] = 'book';
            }
          }

        // Save the node information
        node_save( $node );
        }
        break;


    case 'import_end':
      // Fixup the book node values.
      if ( count($node_fixup) ) {
        foreach ( $node_fixup as $nid => $data) {
          // Get the node
          $n = node_load( $nid );

          // Fixup the book id
          $bid = $data['bid'];
          if ( $bid < 0 ) {
            $bid = _sift_imex_oldnid2newnid(-$bid);
          }

          // Fix up and obtain the parent link id
          $plid = _sift_imex_book_get_plid(
           _sift_imex_oldnid2newnid($data['plid'])
          );

          // Set the values in the node structure
          $n->book['bid'   ] = $bid;
          $n->book['plid'  ] = $plid;
          $n->book['weight'] = $data['weight'];
          $n->book['module'] = 'book';

          // Save the node with the new information.
          node_save( $n );
        }
      }
      break;
    }
  }
}




/**
 * Traverse the book tree to build a list nodes in this book.
 *
 * During the traversal, it is called recursively for each child of the node
 * (in weight, title order).
 *
 * @param $tree
 *   array A subtree of the book menu hierarchy, rooted at the current page.
 * @return
 *   array List of nodes in order.
 */
function _sift_imex_book_export_traverse($tree) {
  $output = array();

  foreach ($tree as $data) {
    // Note- access checking is already performed when building the tree.
    if ($node = node_load($data['link']['nid'], FALSE)) {
      $output[] = $node->nid;
      if ($data['below']) {
        $output = array_merge($output, _sift_imex_book_export_traverse($data['below']));
      }
    }
  }
  return $output;
}




/**
 *
 * Convert parent node value to a menu link value
 * @param integer $plid
 * @return integer
 */
function _sift_imex_book_get_plid( $plid ) {
  $s_sql = <<< SQL
SELECT
  mlid AS plid
FROM
  {book}
WHERE
  nid = %d
SQL;
  $result2 = db_query($s_sql, array( $plid ) );
  while ( $alink = db_fetch_array($result2) ) {
    // Get the mlid value to add to the node information.
    $new_plid = $alink['plid'];
  }
  return $new_plid;
}

/**
 * @}
 */