#!/usr/bin/php
#<?php
// $Id: export_node.php 5291 2008-06-30 15:19:43Z contractor2 $
/**
 * @package Sift
 *
 * @file
 * Provide a method of exporting and importing content
 *
 * @copyright Copyright 2008 Sift Groups
 * See COPYRIGHT.txt and LICENSE.txt.
 */

$filename = $argv[1];
$hostname = $argv[2];
$path = $argv[3];

define(MODULE_NAME, 'export_import_sift');
define(OUTPUTDIR, dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'out');

// Turn off all error reporting
// error_reporting(0);

// This is needed to get the correct settings.php file
$_SERVER['HTTP_HOST'    ] = $hostname;
$_SERVER['SCRIPT_NAME'  ] = '/index.php';

chdir( '/data/drupal' );

require_once './includes/bootstrap.inc';
drupal_bootstrap( DRUPAL_BOOTSTRAP_FULL );

if ( !module_exists( MODULE_NAME )) {
  exit( 'Please enable module: '. MODULE_NAME ."\n" );
}

// act as a valid user
$user = user_load( array( 'uid' => 1 ) );

$dom_document = export_sift_xml( array(
  'node_' . PAGE_CONTENT_TYPE        => 1,
  'node_' . POLL_CONTENT_TYPE        => 1,
  'node_' . STORY_CONTENT_TYP        => 1,
  'node_' . HOUSEPLAN_CONTENT_TYPE   => 1,
  'node_' . FEATURE_CONTENT_TYPE     => 1,
  'node_' . YOURPROJECT_CONTENT_TYPE => 1,
  'node_' . DIRECTORY_CONTENT_TYPE   => 1,
  'node_' . PRODUCT_CONTENT_TYPE     => 1,
  'node_' . IMAGE2_CONTENT_TYPE      => 1,
  )
);

$dom_document->formatOutput = TRUE;
$output = $dom_document->saveXML();

print $output;
$file = fopen( OUTPUTDIR . DIRECTORY_SEPARATOR . $filename, 'w');
fwrite($file, $output);
fclose ($file);
