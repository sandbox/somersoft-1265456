#!/bin/bash
# $Id: import.sh 6471 2008-08-05 10:29:16Z contractor2 $
# @copyright Copyright 2008 Sift Groups
# See COPYRIGHT.txt and LICENSE.txt.
runuser -s /bin/bash lighttpd -c "php -f import_node.php"
